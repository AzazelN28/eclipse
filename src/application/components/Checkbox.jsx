import React, { Component } from "react";
import uuid from "uuid/v4";

import styles from "./Checkbox.styl";

function CheckboxVector({width,height}) {
  const gap = Math.round(Math.min(width,height) * 0.25);
  return (
    <svg className={styles.Checkbox__Box} width={width} height={height} viewBox={`0 0 ${width} ${height}`}>
      <polyline points={`0,0 ${width - gap},0 ${width},${gap} ${width},${height} 0,${height} 0,0`} stroke="white" />
      <g className={styles.Checkbox__Tick}>
        <line x1="0" y1="0" x2="16" y2="16" stroke="white" />
        <line x1="16" y1="0" x2="0" y2="16" stroke="white" />
      </g>
    </svg>
  );
}

export default function Checkbox(props) {
  const id = uuid();
  return (
    <div className={styles.Checkbox}>
      <input type="checkbox" name={id} id={id} defaultChecked={props.checked} />
      <label htmlFor={id}>
        <CheckboxVector width="16" height="16" />
        {props.children}
      </label>
    </div>
  );
}

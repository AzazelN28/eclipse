import Immutable from "immutable";

const initialState = Immutable.Map({
  user: null
});

export default initialState;

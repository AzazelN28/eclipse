import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider as StoreProvider } from "react-redux";
import { I18nextProvider as I18nProvider } from "react-i18next";
import Checkbox from "./components/Checkbox";

import store from "./store";
import i18n from "./i18n";

function Users(props) {
  console.log(props);
  return (
    <h1>Users</h1>
  );
}

function User(props) {
  console.log(props);
  return (
    <h1>User</h1>
  );
}

function Match(props) {
  console.log(props);
  const players = [];
  for (let i = 0; i < 9; i += 1) {
    players.push(
      <div className="Player">
        <img src="//www.placecage.com/200/260" alt="" />
        <div>
          <input type="radio" name={`player-type-${i}`} defaultValue="human" id={`player1typeH${i}`} defaultChecked />
          <label htmlFor={`player1typeH${i}`}>Humano</label>
        </div>
        <div>
          <input type="radio" name={`player-type-${i}`} defaultValue="ia" id={`player1typeI${i}`} />
          <label htmlFor={`player1typeI${i}`}>IA</label>
        </div>
      </div>
    );
  }
  return (
    <div className="MatchDetail">
      <h1>Match</h1>
      <div className="Match">
        <div className="Players">
          {players}
        </div>
        <button type="button">Listo</button>
      </div>
    </div>
  );
}

function MatchPlay(props) {
  console.log(props);
  return (
    <h1>Match Play</h1>
  );
}

function MatchCreate(props) {
  console.log(props);
  return (
    <div className="MatchCreation">
      <h1>Match Creation</h1>
      <div className="MatchCreation__rules">
        <div className="Players">
          6 players
        </div>
        <div className="Input">
          <input type="number" min="1" max="60" name="match-preparation-wait-time" defaultValue="1" />
          <select>
            <option>minutes</option>
            <option>hours</option>
            <option>days</option>
          </select>
        </div>
        <div className="Input">
          <input type="number" min="1" max="60" name="match-action-wait-time" defaultValue="1" />
          <select>
            <option>minutes</option>
            <option>hours</option>
            <option>days</option>
          </select>
        </div>
        <h2>Rules</h2>
        <p>In this screen you can customize game rules</p>
        <h3><Checkbox checked>New Dawn for the Galaxy</Checkbox></h3>
        <ul>
          <li><Checkbox checked>Duplicate factions</Checkbox></li>
          <li><Checkbox checked>Limited Rounds</Checkbox></li>
          <li><Checkbox checked>Plasma Missiles</Checkbox></li>
        </ul>
        <h3><Checkbox checked>Rise of the Ancients</Checkbox></h3>
        <ul>
          <li><Checkbox checked>Rare Technologies</Checkbox></li>
          <li><Checkbox checked>Developments</Checkbox></li>
          <li><Checkbox checked>Ancient Homeworlds</Checkbox></li>
          <li><Checkbox checked>Ancient Hives</Checkbox></li>
          <li><Checkbox checked>Alternative Galactic Center</Checkbox></li>
          <li><Checkbox checked>Warp Portals</Checkbox></li>
          <li><Checkbox checked>Discoveries</Checkbox></li>
          <li><Checkbox checked>Alliances</Checkbox></li>
          <li><Checkbox checked>Extra Reputation Tiles</Checkbox></li>
        </ul>
        <h4>Official variations</h4>
        <ul>
          <li><Checkbox checked>Simultaneous Play</Checkbox></li>
          <li><Checkbox checked>Small Galaxy</Checkbox></li>
          <li><Checkbox checked>Variable Player Order</Checkbox></li>
          <li><Checkbox checked>Ancient Secret Homeworld</Checkbox></li>
          <li><Checkbox checked>Predictable Technologies</Checkbox></li>
        </ul>
        <h3><Checkbox checked>Ship Pack One</Checkbox></h3>
        <ul>
          <li><Checkbox checked>Pass Order</Checkbox></li>
        </ul>
        <h3><Checkbox checked>Shadow of the Rift</Checkbox></h3>
        <ul>
          <li><Checkbox checked>Time Distortion</Checkbox></li>
          <li><Checkbox checked>Evolution</Checkbox></li>
          <li><Checkbox checked>Rare Technologies</Checkbox></li>
          <li><Checkbox checked>Developments</Checkbox></li>
          <li><Checkbox checked>Discoveries</Checkbox></li>
          <li><Checkbox checked>Deep Warp Portals</Checkbox></li>
          <li><Checkbox checked>Anomalies</Checkbox></li>
          <li><Checkbox checked>Special Reputation</Checkbox></li>
        </ul>
        <h3><Checkbox checked>Supernova</Checkbox></h3>
        <ul>
          <li><Checkbox checked>Sectors</Checkbox></li>
          <li><Checkbox checked>Discovery</Checkbox></li>
        </ul>
        <h3><Checkbox checked>Pulsar</Checkbox></h3>
        <ul>
          <li><Checkbox checked>Sectors</Checkbox></li>
          <li><Checkbox checked>Discovery</Checkbox></li>
        </ul>
        <h3><Checkbox checked>Nebula</Checkbox></h3>
        <ul>
          <li><Checkbox checked>Sectors</Checkbox></li>
          <li><Checkbox checked>Discovery</Checkbox></li>
        </ul>
        <h3><Checkbox checked>Black hole</Checkbox></h3>
        <ul>
          <li><Checkbox checked>Sectors</Checkbox></li>
          <li><Checkbox checked>Discovery</Checkbox></li>
        </ul>
      </div>
    </div>
  );
}

function Matches(props) {
  console.log(props);
  return (
    <h1>Matches</h1>
  );
}

function Home(props) {
  console.log(props);
  return (
    <h1>Home</h1>
  );
}

export default function Application() {
  return (
    <StoreProvider store={store}>
      <I18nProvider i18n={i18n}>
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />

            <Route path="/users" component={Users} />
            <Route path="/users/:user" component={User} />

            <Route path="/matches/create" component={MatchCreate} />
            <Route path="/matches/:match/play" component={MatchPlay} />
            <Route path="/matches/:match" component={Match} />
            <Route path="/matches" component={Matches} />
          </Switch>
        </Router>
      </I18nProvider>
    </StoreProvider>
  );
}

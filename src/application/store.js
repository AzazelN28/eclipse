import { createStore } from "redux";
import { combineReducers } from "redux-immutable";
import thunk from "redux-thunk";

import initialState from "./initialState";
import reducers from "./reducers";

const store = createStore(
  combineReducers(reducers),
  initialState,
  /* eslint-disable no-underscore-dangle */
  /** @see https://github.com/zalmoxisus/redux-devtools-extension */
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

export default store;

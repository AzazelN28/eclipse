import i18next from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

const i18n = i18next
  .use(LanguageDetector)
  .init({
    resources: {

      es: {
        translations: {

        },
      },

      en: {
        translations: {

        },
      },

    },

    fallbackLng: "en",

    ns: ["translations"],
    defaultNS: "translations",

    keySeparator: false,

    interpolation: {
      escapeValue: false,
      formatSeparator: ",",
    },

    react: {
      wait: true,
    },
  });

export default i18n;

import React, { Component } from "react";
import { render } from "react-dom";

import Application from "~/application/Application";
import Game from "~/game/Game";

import Factions from "~/game/factions/Factions";

import Player from "~/game/players/Player";

import Match from "~/game/matches/Match";
import MatchSettings from "~/game/matches/MatchSettings";

console.log(Factions);

const settings = new MatchSettings();
const match = new Match(settings);
match.players.push(new Player(Factions.find(faction => faction.id === "eridani")));
match.players.push(new Player(Factions.find(faction => faction.id === "hydran")));

Game.prepare(match);

console.log(match);

render(React.createElement(Application), document.querySelector("#ApplicationContainer"));

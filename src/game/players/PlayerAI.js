/**
 * This is strongly based on Over 2 million different AIs for Eclipse. (version 1.1) By Vica8081
 */
const MainTrait = {
  /**
   * Would explore sector 2, then sector 3, then 2 if available, 1 if available,
   * then 3. Would try to exchange diplomat with every player it meet, would never start
   * a war. If attacked two option (flip a coin) would fight to death with their aggressor or
   * just until it get one hex of the opponent and then would try to settle peace. Monolith
   * and orbitals are priority researches for a pacifist.
   * Roleplaying notes: This AI likes to play multiplayer solitaires and don’t want to get
   * messed up with battles, just grow as much as you can and then build monolith. Not
   * the funniest AI but it serves as a benchmark to others.
   */
  PACIFIST: "pacifist",
  /**
   * Would explore sector 2, then sector 3. The AI would always try to put the
   * explored hex so it became isolated of the rest of players. Would try to exchange
   * diplomats with every player it meet, and would never start a war until it would be
   * unable to explore further. Then will go out of its shell and attack the weakest
   * neighbor opponent. Monolith and orbitals are priority researches for a turtle. If
   * attacked when there is still exploration available would fight until it get one hex of
   * the opponent and then would try to settle peace, when there is not exploration would
   * fight to death.
   * Roleplaying notes: This AI plays a lot of starcraft games so it like to grow
   * undisturbed at the beginning and when is have gotten big and powerful, storm into
   * the rest galaxy. Is not a bad strategy so if left unchecked could easily get the victory,
   * so it is a good AI to play against
   */
  TURTLE: "turtle",
  /**
   * Would explore sector 2, then sector 3, then 2 if available, 1 if
   * available, then 3. Would try to exchange diplomat with every player it meet, but
   * would attack to the closest player which is already involved in any war (so if B
   * attack C the opportunist would attack B or C according which is closer to it, if both
   * are close, would attack the weakest player). Would only have one war at time and
   * would seek for peace if things are going bad.
   * Roleplaying notes: This AI plays a lot of diplomacy games. It seems a pacifist but
   * then in the worst moment, when you have your ships far away, backstab you and
   * grab your precious system. In my playtesting (quite limited) is the more successful
   * AI, so also a good AI to play against
   */
  OPPORTUNIST: "opportunist",
  /**
   * Would explore sector 2, then sector 3, then 2 if available, 1 if
   * available, then 3. Would try to exchange diplomat with every player it meet, but if at
   * any time one neighbor player have less than 66% of its own fleet would attack. If the
   * other player have less than 80% of its fleet and the technology is worse in at least
   * two components (shield, computer, guns, etc.) would also attack. If there is not
   * exploration available, would attack its weakest neighbor. Would only have one war
   * at time and would seek for peace if things are going bad
   * Roleplaying notes: This AI is also a wolf in a sheep (pacifist) dress. I´m now
   * playtesting to fuse Pacifist/Opportunist/Greedy Neighbor as one single AI that act as
   * pacifist but when opportunity arise you roll a die to determine if this blue player is
   * really a pacifist (1-2) so don’t attack you, or (3-4) opportunist consider you
   * backstabbed or (5-6) a greedy neighbor don’t forget keep your army full. This way
   * the human player couldn’t possibly know from the beginning what AI player to trust
   * and what AI player to watch.
   */
  GREEDY: "greedy", // -> Greedy Neighbour
  /**
   * Would explore sector 2, then sector 1, then sector 2 if available, 1 if
   * available, then sector 3. Would try to exchange diplomats with every other player,
   * and don’t get involved in any war with them but hate ancients and try to kill as many
   * ancients as possible. First would kill all the ancients in their borders but when the
   * only remaining ancients are behind other players then war is the only option, but
   * would try to seek peace as soon as the ancients are destroyed/the pass is achieved.
   * When/if all ancients are destroyed the cleaner would behave as a pacifist and could
   * even get an optional bonus of VP (maybe 5?)
   * Roleplaying notes: This AI is another of the winning AI in my (limited) playtesting,
   * and in fact is a multiplayer solitaire player looking for some action, although you can
   * also imagine them as a former slaved races of the ancients or something like that
   */
  CLEANER: "cleaner",
  /**
   * Would explore sector 2, then sector 1, then sector 2 if available, 1 if
   * available, then sector 3. Would never exchange diplomats and would attack the
   * weaker player close to it, although it would have only one war at time. If thing are
   * going bad, would try to make peace and attack other neighbor player (but only if the
   * other player is seen as weaker)
   * Roleplaying notes: This AI think that the only acceptable form of life it is their own,
   * (like Borgs) so every player is an abomination and a enemy, although is wise enough
   * to attack them one at time, but I doubt it can ever win a game of Eclipse. Noneless is
   * a fun AI to play
   */
  OFFENSIVE: "offensive",
  /**
   * Would explore sector 2, then sector 1, then sector 2 if available, 1
   * if available, then sector 3. At first would attack the weaker player close to it. Then
   * will exchange diplomats with the rest. When it have an average of at least 2,5 points
   * in every medal space, would switch to a pacifist behaviour (with orbital and monolith
   * as priority technology included) and try to make peace. If thing are going bad, and
   * still under the 2,5 average would try to make peace and attack other neighbor player
   * (but only if the other player is weaker).
   * Roleplaying notes: This AI is a eclipse gamer. It try to get early combat points and at
   * soon as the combat points provide little chance to increase (the 2.5 average) switch to
   * a pacific behavior. However in my (limited) playtesting this AI have never won.
   */
  COLLECTOR: "collector", // -> Medal collector
  /**
   * Would explore sector 2, then sector 1, then sector 2 if available,
   * 1 if available, then sector 3. At first would exchange diplomats with others players,
   * but when two diplomats are exchanged would attack to death to the next player
   * which become contacted. Would also attack to death to any player which have not
   * exchanged diplomats (the first version would wait to exchange it 3 diplomats before
   * war, but in playtesting it would never contact a 4th player, so it was changed, but if
   * you play with 8-9 players, maybe the first version is better, and more in spirit with
   * the AI). If only contacted two players (so not war) and there is not exploration
   * available, would attack the last player contacted.
   * Roleplaying notes: This AI is a bit paranoid so if it is unable to exchange diplomats,
   * it would fear that the other player is going to attack them, so it prefer to give the first
   * blow without asking.
   */
  PARANOID: "paranoid", // -> Preventive Attack
  /**
   * A risk player would hate the player with that specific color (so a
   * RISK (red) would try to kill and annihilate the red player). The AI would explore to
   * get connected to the hated player, and then would only explore sector 3. Would
   * exchange diplomats with the other players, but attack to death to its objective. A
   * RISK player doesn’t care about VP, planets controlled or what the other players are
   * doing, its only objective is to rid the galaxy of the horrible menace of red color, so in
   * some ways a RISK player is not really playing eclipse and a moral victory (or bonus
   * points) is achieved if the RISK player is able to completely destroy its hated color,
   * and save the galaxy of this awful menace. When/if that is achieved the RISK player
   * would switch to a pacifist behavior (with orbital and monolith as preferred
   * technologies).
   * Roleplaying Notes: This AI is clearly playing the wrong game. It is fun to watch, and
   * if you want and additional challenge you can put one (or even two) risk player
   * against yourself. If you are able to survive, or even to win the game, then you are a
   * damn good player.
   */
  HATER: "hater", // -> RISK (color)
  /**
   * Would explore sector 2, then sector 1, then sector 2 if available, 1 if
   * available, then sector 3. A gamer in the other hand is playing eclipse. At beginning of
   * each turn it would count the VP of every player. If nobody have the same or more
   * than its, would follow the peace flow, and monolith (not orbitals) would be preferred
   * technologies. If somebody have equal or more VP than its, then would attack the
   * player with most VP, because it’s the leader and is going to win (not matter than its
   * army is double than him, for example). Would never make peace with somebody
   * with more VP than its (although is losing) because its didn’t like to become second.
   * Roleplaying notes: This AI is clearly a real gamer. In real life nobody in its own
   * volition would attack the strongest country (so is bigger and stronger than you) just
   * because they are the strongest. People tend to pick the weaker player and there is a
   * reason to. So a gamer alone is doomed, but a gamer and a couple of opportunist or
   * several gamers can make an interesting challenge. It make also a good AI to play
   * against because is like a RISK but only goes against you when you are strong enough
   * and not from the beginning. And at least have a fair reason to fight you, not just I
   * hate you, red color.
   */
  GAMER: "gamer",
};

const UpgradeTrait = {
  TRUE_BALANCED: "true-balanced",
  BALANCED_ARMOR: "balanced-armor",
  BALANCED_GUNS: "balanced-guns",
  SPECIALIZED_GUNS: "specialized-guns",
  SPECIALIZED_ARMOR: "specialized-armor",
  SPECIALIZED_COMPUTER: "specialized-computer",
  SPECIALIZED_SHIELD: "specialized-shield",
  SPECIALIZED_ENGINE: "specialized-engine"
};

const BuildingTrait = {
  BALANCED_DREADNOUGHT: "balanced-dreadnought",
  BALANCED_CRUISER: "balanced-cruiser",
  ULTRADEFENSIVE_DREADNOUGHT: "ultradefensive-dreadnought",
  ULTRADEFENSIVE_CRUISER: "ultradefensive-cruiser",
  DEFENSIVE_DREADNOUGHT: "defensive-dreadnought",
  DEFENSIVE_CRUISER: "defensive-cruiser",
  OFFENSIVE_DREADNOUGHT: "offensive-dreadnought",
  OFFENSIVE_CRUISER: "offensive-cruiser",
  OFFENSIVE_MIX: "offensive-mix",
  MOBILE_DREADNOUGHT: "mobile-dreadnought",
  MOBILE_CRUISER: "mobile-cruiser",
};

const UpgradeRateTrait = {
  TECH_ADDICT: "tech-addict",
  BALANCED: "balanced",
  BUDGET_LIMITED: "budget-limited",
};

const ChartFlowTrait = {
  OBSESSED: "obsessed",
  THOROUGHLY: "thoroughly",
};

const WarTrait = {
  SUPERIOR_TECH: "superior-tech",
  BIG_FORCE: "big-force",
};

const ExplorationTrait = {
  GROW_AND_PROSPER: "grow-and-prosper",
  WE_ARE_THE_BIGGEST: "we-are-the-biggest",
  WE_ARE_NOT_SMALLEST: "we-are-not-smallest",
};

const DiscardTrait = {
  NEVER: "never",
  ONLY_ONE_ANCIENT: "only-one-ancient",
  ONLY_TWO_ANCIENTS: "only-two-ancients",
};

const DiscoveryTrait = {
  ALWAYS_VP: "always-vp",
  ALWAYS_VP_EXCEPT_UPGRADE: "always-vp-except-upgrade",
  NEVER_VP: "never-vp",
};

export default class PlayerAI {
  constructor() {
    this.traits = {
      main: MainTrait.PACIFIST,
      upgrade: UpgradeTrait.TRUE_BALANCED,
      upgradeRate: UpgradeRateTrait.BALANCED,
      chartFlow: ChartFlowTrait.OBSESSED,
      war: WarTrait.SUPERIOR_TECH,
      exploration: ExplorationTrait.GROW_AND_PROSPER,
      discard: DiscardTrait.NEVER,
      discovery: DiscoveryTrait.ALWAYS_VP,
    };
  }
}

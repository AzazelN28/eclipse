import Game from "~/game/Game";

import PlayerType from "./PlayerType";

export default class Player {
  constructor(faction) {
    this.faction = faction;
    this.type = PlayerType.HUMAN;
    this.hasLost = false;
    this.hasPassed = false;
    this.population = {
      money: 0,
      science: 0,
      materials: 0,
    };
    this.resources = {
      money: 0,
      science: 0,
      materials: 0,
    };
    this.availableColonyShips = faction.colonyShips;
    this.technologies = []; // Current technologies
    this.influence = faction.initialInfluence;
    this.actions = []; // Actions in this round
    this.shipDesigns = []; // Ship designs
    this.shipUnits = []; // Ships and their location
    this.sectors = []; // Sectors and their location
    this.reputationPoints = [];
    this.availableAmbassadors = []; //
  }

  getBalance() {
    return this.resources.money
      + Game.getPopulationProduction(this.population.money)
      - Game.getInfluenceCost(this.influence);
  }
}

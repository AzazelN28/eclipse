export const PlayerType = {
  HUMAN: "human",
  COMPUTER: "computer",
};

export default PlayerType;

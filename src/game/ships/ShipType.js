export const ShipType = {
  INTERCEPTOR: "interceptor",
  CRUISER: "cruiser",
  DREADNOUGHT: "dreadnought",
  STARBASE: "starbase"
};

export default ShipType;

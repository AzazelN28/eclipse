

export default class Ship {
  constructor() {
    this.id = null;
    this.name = "";
    this.type = null;
    this.cost = 0;
    this.base = {
      initiative: 0,
      hull: 0,
      energy: 0,
      hit: 0,
      damage: 0
    };
    this.parts = [];
  }
}

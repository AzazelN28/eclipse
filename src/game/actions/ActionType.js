const ActionType = {
  EXPLORE: "explore",
  INFLUENCE: "influence",
  RESEARCH: "research",
  UPGRADE: "upgrade",
  BUILD: "build",
  MOVE: "move",
  PASS: "pass",
};

export default ActionType;

export default class Action {
  constructor(player, type, parameters) {
    this.player = player;
    this.type = type;
    this.parameters = parameters;
  }
}

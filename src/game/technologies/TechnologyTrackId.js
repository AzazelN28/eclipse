export const TechnologyTrackId = {
  MILITARY: "military",
  ADVANCED: "advanced",
  NANOTECH: "nanotech",
  RARE: "rare",
};

export default TechnologyTrackId;

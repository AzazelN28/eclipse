export const MatchPhase = {
  PREPARATION: "preparation",
  ACTION: "action",
  COMBAT: "combat",
  UPKEEP: "upkeep",
  CLEANUP: "cleanup"
};

export default MatchPhase;

import MatchPhase from "./MatchPhase";
import MatchDirection from "./MatchDirection";

export default class Match {
  constructor(settings) {
    this.settings = settings;
    // Represents the round we are playing right now.
    this.round = 0;
    // Represents the current phase of the game, by default
    // it is PREPARATION, which indicates to the Game module
    // that it should prepare this match.
    this.phase = MatchPhase.PREPARATION;
    // Direction of play (see Rise of the Ancients rules).
    this.direction = MatchDirection.LEFT;
    // Represents all the players that are playing the match.
    this.players = [];
    // Represents the bag of reputation tiles
    this.reputations = [];
    // Represents the bag of technologies
    this.technologies = [];
    // Represents the stack of discoveries
    this.discoveries = [];
    // Represents the bag of developments
    this.developments = [];
    // Represents the stack of sectors
    this.sectors = {
      inner: [],
      middle: [],
      outer: [],
    };
    // Represents the technology track.
    this.availableTechnologies = [];
    // Represents the map, it's an unordered list of hex tiles.
    this.map = [];
    // Represents which player begins the match.
    this.initialPlayerIndex = 0;
    // Represents which player is playing right now.
    this.player = null;
    // Represents the current player index.
    this.playerIndex = 0;
  }
}

import MatchPhase from "./MatchPhase";

export const MatchPhaseTransition = {
  [MatchPhase.PREPARATION]: [MatchPhase.ACTION],
  [MatchPhase.ACTION]: [MatchPhase.COMBAT, MatchPhase.UPKEEP],
  [MatchPhase.COMBAT]: [MatchPhase.UPKEEP],
  [MatchPhase.UPKEEP]: [MatchPhase.CLEANUP]
};

export default MatchPhaseTransition;

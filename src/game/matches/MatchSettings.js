export default class MatchSettings {
  constructor() {
    // Eclipse: New Dawn for the Galaxy
    this.base = {
      // Enables or disables plasma missiles.
      useMissiles: true,
      // Modifying this parameters you can modify how much reputation can be
      // draw from the reputation bag.
      reputation: {
        perBattle: 1,
        perInterceptor: 1,
        perStarBase: 1,
        perCruiser: 2,
        perDreadnought: 3,
        perAncientCruiser: 1,
        perAncientStarbase: 3,
      },
      // Number of rounds (by default are 9 but the players can modify this value or make it Infinite by setting this to 0).
      maxRounds: 9,
    };

    // Eclipse: Rise of the Ancients
    this.riseOfTheAncients = {
      useRareTechnologies: true,
      useDevelopments: true,
      useAncientHomeworlds: true,
      useAlternativeGalacticCenter: true,
      useAncientHives: true,
      useWarpPortals: true,
      useDiscoveries: true,
      useAlliances: true,
      useReputation: true,

      // RotA - Official variants
      useSimultaneousPlay: true,
      useVariablePlayerOrder: true,
      useSmallGalaxy: true,
      useAncientSecretHomeworld: true,
      howManyAncientSecretHomeworlds: 1,
      predictableTechnologies: true,
    };

    // Eclipse: Ship Pack One
    this.shipPackOne = {
      usePassOrder: true,
    };

    // Eclipse: Shadow of the Rift
    this.shadowOfTheRift = {
      useTimeDistortion: true,
      useEvolution: true,
      useRareTechnologies: true,
      useDevelopments: true,
      useDeepWarpPortals: true,
      useAnomalies: true,
      useDiscoveries: true,
      useSpecialReputation: true,
    };

    // Eclipse: Supernova
    this.supernova = {
      useSectors: true,
      useDiscovery: true,
    };

    // Eclipse: Pulsar
    this.pulsar = {
      useSectors: true,
      useDiscovery: true,
    };

    // Eclipse: Nebula
    this.nebula = {
      useSectors: true,
      useDiscovery: true,
    };

    // Eclipse: Black Hole
    this.blackHole = {
      useSectors: true,
      useDiscovery: true,
    };
  }
}

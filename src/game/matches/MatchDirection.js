export const MatchDirection = {
  /** Normal play direction */
  LEFT: "left",
  /** Inverse play direction */
  RIGHT: "right",
};

export default MatchDirection;

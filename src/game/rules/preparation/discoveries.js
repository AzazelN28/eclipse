export default function prepareDiscoveries(match) {
  // Eclipse: New Dawn for the Galaxy - Discoveries
  // add every discovery
  match.discoveries.push(
    "money", "money", "money",
    "science", "science", "science",
    "materials", "materials", "materials",
    "ancient-technology", "ancient-technology", "ancient-technology",
    "ancient-cruiser", "ancient-cruiser", "ancient-cruiser",
    "axion-computer",
    "hypergrid-source",
    "shard-hull",
    "ion-turret",
    "conformal-drive",
    "flux-shield",
  );

  // Eclipse: Supernova discovery
  if (match.settings.supernova.useDiscovery) {
    match.discoveries.push(
      "ion-missile",
    );
  }

  // Eclipse: Pulsar discovery
  if (match.settings.pulsar.useDiscovery) {
    match.discoveries.push(
      "nova-burst",
    );
  }

  // Eclipse: Rise of the Ancients - New discoveries
  if (match.settings.riseOfTheAncients.useDiscoveries) {
    match.discoveries.push(
      "jump-drive",
      "ion-disruptor",
      "muon-source",
      "morph-shield",
      "money-science-materials",
      "ancient-orbital",
    );
  }

  if (match.settings.riseOfTheAncients.useWarpPortals) {
    match.discoveries.push(
      "ancient-warp-portal",
    );
  }
}

import Random from "~/game/Random";
import TechnologyTrackId from "~/game/technologies/TechnologyTrackId";

function getInitialTechnologies(match) {
  switch (match.players.length) {
    default: throw new Error("Invalid amount of players, it should be >= 2 and <= 9");
    case 2:
      return 12;
    case 3:
      return 14;
    case 4:
      return 16;
    case 5:
      return 18;
    case 6:
      return 20;
    case 7:
      return 22;
    case 8:
      return 24;
    case 9:
      return 26;
  }
}


export default function prepareTechnologies(match) {
  // First of all, we prepare the arrays for the match.
  // There are four technology tiles of each track, except for the
  // rare technologies.
  // Eclipse: New Dawn for the Galaxy - Technologies
  match.technologies.push(
    "neutron-bombs", "neutron-bombs", "neutron-bombs", "neutron-bombs",
    "starbase", "starbase", "starbase", "starbase",
    "plasma-cannon", "plasma-cannon", "plasma-cannon", "plasma-cannon",
    "phase-shield", "phase-shield", "phase-shield", "phase-shield",
    "advanced-mining", "advanced-mining", "advanced-mining", "advanced-mining",
    "tachyon-source", "tachyon-source", "tachyon-source", "tachyon-source",
    "gluon-computer", "gluon-computer", "gluon-computer", "gluon-computer",
    "gauss-shield", "gauss-shield", "gauss-shield", "gauss-shield",
    "improved-hull", "improved-hull", "improved-hull", "improved-hull",
    "fusion-source", "fusion-source", "fusion-source", "fusion-source",
    "positron-computer", "positron-computer", "positron-computer", "positron-computer",
    "advanced-economy", "advanced-economy", "advanced-economy", "advanced-economy",
    "tachyon-drive", "tachyon-drive", "tachyon-drive", "tachyon-drive",
    "antimatter-cannon", "antimatter-cannon", "antimatter-cannon", "antimatter-cannon",
    "quantum-grid", "quantum-grid", "quantum-grid", "quantum-grid",
    "nanorobots", "nanorobots", "nanorobots", "nanorobots",
    "fusion-drive", "fusion-drive", "fusion-drive", "fusion-drive",
    "advanced-robotics", "advanced-robotics", "advanced-robotics", "advanced-robotics",
    "orbital", "orbital", "orbital", "orbital",
    "advanced-labs", "advanced-labs", "advanced-labs", "advanced-labs",
    "monolith", "monolith", "monolith", "monolith",
    "artifact-key", "artifact-key", "artifact-key", "artifact-key",
    "wormhole-generator", "wormhole-generator", "wormhole-generator", "wormhole-generator",
  );

  // Eclipse: New Dawn for the Galaxy - Plasma Missiles
  // If the player has enabled the use of missiles, then
  // should add them to the technology bag.
  if (match.settings.base.useMissiles) {
    match.technologies.push(
      "plasma-missile", "plasma-missile", "plasma-missile", "plasma-missile",
    );
  }

  // Eclipse: Rise of the Ancients - Rare Technologies
  // If the player has enabled the use of rare technologies,
  // then add them to the technology bag.
  if (match.settings.riseOfTheAncients.useRareTechnologies) {
    match.technologies.push(
      "antimatter-splitter",
      "neutron-absorber",
      "distortion-shield",
      "cloaking-device",
      "point-defense",
      "conifold-field",
      "sentient-hull",
      "interceptor-bay",
      "flux-missile",
      "zero-point-source",
    );
  }

  // Eclipse: Shadow of the Rift
  if (match.settings.shadowOfTheRift.useRareTechnologies) {
    match.technologies.push(
      "advanced-genetics",
      "metasynthesis",
      "soliton-cannon",
      "rift-cannon",
      "transition-drive",
      "absorption-shield",
    );
  }

  // Gets the number of technologies based on the number players
  // playing this match.
  let initialTechnologyCount = getInitialTechnologies(match);
  while (initialTechnologyCount > 0) {
    const technology = Random.default.takeOne(match.technologies);
    if (technology.track !== TechnologyTrackId.RARE) {
      initialTechnologyCount -= 1;
    }
    match.availableTechnologies.push(technology);
  }
}

/**
 * Adds the specified quantity of reputation tiles of each
 * type into the reputation bag of the match.
 *
 * @param {Match} match
 * @param {Number} four
 * @param {Number} three
 * @param {Number} two
 * @param {Number} one
 */
function addReputation(match, four, three, two, one) {
  for (let index = 0; index < four; index += 1) {
    match.reputations.push(4);
  }
  for (let index = 0; index < three; index += 1) {
    match.reputations.push(3);
  }
  for (let index = 0; index < two; index += 1) {
    match.reputations.push(2);
  }
  for (let index = 0; index < one; index += 1) {
    match.reputations.push(1);
  }
}

/**
 * Adds the reputation tiles into the reputation bag.
 *
 * @param {Match} match
 */
export default function prepareReputation(match) {
  addReputation(match, 4, 7, 9, 12);
  if (match.settings.riseOfTheAncients.useReputation) {
    addReputation(match, 2, 3, 2, 2);
  }
}

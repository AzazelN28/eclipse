import Sectors from "~/game/sectors/Sectors";
import SectorTile from "~/game/sectors/SectorTile";

import Random from "~/game/Random";

/**
 *
 *
 * @param {Number} id
 * @return {Sector}
 */
function findSectorById(id) {
  return Sectors.find(sector => sector.id === id);
}

function filterInnerSectors(sectorTile) {
  return sectorTile.sector.id >= 100 && sectorTile.sector.id < 200;
}

function filterMiddleSectors(sectorTile) {
  return sectorTile.sector.id >= 200 && sectorTile.sector.id < 300;
}

function filterOuterSectors(sectorTile) {
  return sectorTile.sector.id >= 300;
}

/**
 *
 * @param {Match} match
 * @return {Array}
 */
function getInitialSectorPositionsForAncientHomeworlds(match) {
  switch (match.players.length) {
    case 2:
      return [ // initial coordinates
        [2, -2, 0, 0, "ancient"], // Ancient Homeworld
        [2, 0, -2, 0, "ancient"], // Ancient Homeworld
        [-2, 2, 0, 0, "ancient"], // Ancient Homeworld
        [-2, 0, 2, 0, "ancient"], // Ancient Homeworld
        [0, -2, 2, 0, "player"],
        [0, 2, -2, 0, "player"],
      ];

    case 3:
      return [
        [2, -2, 0, 0, "ancient"], // Ancient Homeworld
        [2, 0, -2, 0, "player"],
        [-2, 2, 0, 0, "player"],
        [-2, 0, 2, 0, "ancient"], // Ancient Homeworld
        [0, -2, 2, 0, "player"],
        [0, 2, -2, 0, "ancient"], // Ancient Homeworld
      ];

    case 4:
      return [ // initial coordinates
        [2, -2, 0, 0, "player"],
        [2, 0, -2, 0, "ancient"], // Ancient Homeworld
        [-2, 2, 0, 0, "player"],
        [-2, 0, 2, 0, "ancient"], // Ancient Homeworld
        [0, -2, 2, 0, "player"],
        [0, 2, -2, 0, "player"],
      ];

    case 5:
      return [
        // initial coordinates
        [2, -2, 0, 0, "player"],
        [2, 0, -2, 0, "player"],
        [-2, 2, 0, 0, "player"],
        [-2, 0, 2, 0, "player"],
        [0, -2, 2, 0, "player"],
        [0, 2, -2, 0, "ancient"], // Ancient Homeworld
      ];

    case 6:
      throw new Error("A six player game cannot have ancient homeworlds");

    case 7:
      return [
        /* initial coordinates */
        [-3, 1, 2, 0, "player"],
        [2, -3, 1, 0, "player"],
        [-1, 3, -2, 0, "player"],
        [-2, -1, 3, 0, "player"],
        [1, 2, -3, 0, "player"],
        [-3, 3, 0, 0, "ancient"], // Ancient Homeworld (7)
        [3, 0, -3, 0, "player"],
        [0, -3, 3, 0, "player"],
      ];

    case 8:
      return [
        /* initial coordinates */
        [3, -2, -1, 0, "ancient"], // Ancient Homeworld (7 y 8)
        [-3, 1, 2, 0, "player"],
        [2, -3, 1, 0, "player"],
        [-1, 3, -2, 0, "player"],
        [-2, -1, 3, 0, "player"],
        [1, 2, -3, 0, "player"],
        [-3, 3, 0, 0, "ancient"], // Ancient Homeworld (7)
        [3, 0, -3, 0, "player"],
        [0, -3, 3, 0, "player"],
      ];

    case 9:
      throw new Error("A nine player game cannot have ancient homeworlds");

    default:
      throw new Error("Invalid amount of players, it should be >= 2 and <= 9");
  }
}

/**
 *
 * @param {Match} match
 * @return {Array}
 */
function getInitialSectorPositions(match) {
  if (match.settings.riseOfTheAncients.useAncientHomeworlds) {
    return getInitialSectorPositionsForAncientHomeworlds(match);
  }

  switch (match.players.length) {
    case 2:
      return [
        /* initial coordinates */
        [0, 2, -2, 0, "player"],
        [0, -2, 2, 0, "player"],
      ];

    case 3:
      return [
        /* initial coordinates */
        [0, 2, -2, 3, "player"],
        [2, -2, 0, 2, "player"],
        [-2, 0, 2, 1, "player"],
      ];

    case 4:
      return [
        /* initial coordinates */
        [2, -2, 0, 1, "player"],
        [2, 0, -2, 1, "player"],
        [-2, 2, 0, 1, "player"],
        [-2, 0, 2, 1, "player"],
      ];

    case 5:
      return [
        /* initial coordinates */
        [2, -2, 0, 1, "player"],
        [2, 0, -2, 0, "player"],
        [-2, 2, 0, 2, "player"],
        [-2, 0, 2, 1, "player"],
        [0, 2, -2, 0, "player"], // NOTE: If you want to made the disposition random, you can comment this, instead the other line
      ];

    case 6:
      return [
        /* initial coordinates */
        [2, -2, 0, 2, "player"],
        [2, 0, -2, 1, "player"],
        [-2, 2, 0, 2, "player"],
        [-2, 0, 2, 1, "player"],
        [0, -2, 2, 0, "player"],
        [0, 2, -2, 0, "player"],
      ];

    case 7:
      return [
        /* initial coordinates */
        [3, -1, -2, 0, "player"],
        [-3, 1, 2, 0, "player"],
        [-2, 3, -1, 0, "player"],
        [2, -3, 1, 0, "player"],
        [-2, -1, 3, 0, "player"],
        [1, 2, -3, 0, "player"],
        [0, -3, 3, 0, "player"],
      ];

    case 8:
      return [
        /* initial coordinates */
        [3, -1, -2, 0, "player"],
        [-3, 1, 2, 0, "player"],
        [-2, 3, -1, 0, "player"],
        [2, -3, 1, 0, "player"],
        [2, 1, -3, 0, "player"],
        [-2, -1, 3, 0, "player"],
        [0, 3, -3, 0, "player"],
        [0, -3, 3, 0, "player"],
      ];

    case 9:
      return [
        /* initial coordinates */
        [3, -2, -1, 0, "player"], // Ancient Homeworld (7 y 8)
        [-3, 1, 2, 0, "player"],
        [2, -3, 1, 0, "player"],
        [-1, 3, -2, 0, "player"],
        [-2, -1, 3, 0, "player"],
        [1, 2, -3, 0, "player"],
        [-3, 3, 0, 0, "player"], // Ancient Homeworld (7)
        [3, 0, -3, 0, "player"],
        [0, -3, 3, 0, "player"],
      ];

    default:
      throw new Error("Invalid amount of players, it should be >= 2 and <= 9");
  }
}

function getMiddleSector(match) {
  return 12;
}

function getOuterSector(match) {
  switch (match.players.length) {
    default: throw new Error("Invalid amount of players, it should be >= 2 and <= 9");
    case 2:
      return 5;
    case 3:
      return 10;
    case 4:
      return 14;
    case 5:
      return 16;
    case 6:
      return 18;
    case 7:
      return 22;
    case 8:
      return 24;
    case 9:
      return 24;
  }
}

/**
 * This method prepares the sectors array, in the boardgame we prepare three separate
 * stacks, one for the inner sectors, one for the middle sectors and one for the
 * outer sectors.
 *
 * @param {Match} match
 */
export default function prepareSectors(match) {
  const sectors = new Array();

  // Eclipse: Supernova sectors
  if (match.settings.supernova.useSectors) {
    sectors.push(
      new SectorTile(findSectorById(391)),
      new SectorTile(findSectorById(392)),
    );
  }

  // Eclipse: Pulsar sectors
  if (match.settings.pulsar.useSectors) {
    sectors.push(
      new SectorTile(findSectorById(393)),
      new SectorTile(findSectorById(394)),
    );
  }

  // Eclipse: Rise of the Ancients - Ancient Hives
  // If we use ancient hives, then we add these
  // tiles to the stack of sectors.
  if (match.settings.riseOfTheAncients.useAncientHives) {
    sectors.push(
      new SectorTile(findSectorById(212)),
      new SectorTile(findSectorById(319)),
    );
  }

  // Eclipse: Rise of the Ancients - Warp portals
  if (match.settings.riseOfTheAncients.useWarpPortals) {
    sectors.push(
      new SectorTile(findSectorById(281)),
      new SectorTile(findSectorById(381)),
      new SectorTile(findSectorById(382)),
    );
  }

  sectors.push(...Sectors.map(sector => new SectorTile(sector)));

  // Prepare the sector stacks.
  match.sectors.inner = Random.default.shuffle(sectors.filter(filterInnerSectors));
  match.sectors.middle = Random.default.shuffle(sectors.filter(filterMiddleSectors)).slice(0, getMiddleSector(match));
  match.sectors.outer = Random.default.shuffle(sectors.filter(filterOuterSectors)).slice(0, getOuterSector(match));

  if (match.settings.riseOfTheAncients.useAlternativeGalacticCenter) {
    // TODO: See how we can override the values from the
    // default galactic center to add different ships.
    match.map.push(new SectorTile(findSectorById(1), 0, 0, 0));
  } else {
    match.map.push(new SectorTile(findSectorById(1), 0, 0, 0));
  }

  // Eclipse: Rise of the Ancients
  // Using Ancient Homeworlds.
  if (match.settings.riseOfTheAncients.useAncientHomeworlds &&
    match.players.length !== 6 &&
    match.players.length !== 9) {
    const positions = getInitialSectorPositionsForAncientHomeworlds(match);
    let playerIndex = 0;
    for (let sectorIndex = 0; sectorIndex < positions.length; sectorIndex++) {
      const [x, y, z, direction, type] = positions[sectorIndex];
      if (type === "player") {
        const player = match.players[playerIndex];
        playerIndex += 1;
        const sector = findSectorById(player.faction.initialSector);
        const sectorTile = new SectorTile(sector, x, y, z, direction, player);
        match.map.push(sectorTile);
      } else {
        // TODO: Add Ancients sectors.
        // const sector = findSectorById(player.faction.initialSector);
        // const sectorTile = new SectorTile(sector, x, y, z, direction);
        // match.map.push(sectorTile);
      }
    }
  } else {
    const positions = getInitialSectorPositions(match);
    for (let sectorIndex = 0; index < positions.length; index++) {
      const [x, y, z, direction, type] = positions[sectorIndex];
      const player = match.players[sectorIndex];
      const sector = findSectorById(player.faction.initialSector);
      const sectorTile = new SectorTile(sector, x, y, z, direction, player);
      match.map.push(sectorTile);
    }
  }

}

import Random from "~/game/Random";

export default function prepare(match) {
  // Eclipse: Rise of the Ancients - Developments
  if (match.settings.riseOfTheAncients.useDevelopments) {
    const developments = [
      "ancient-monument",
      "artifact-link",
      "diplomatic-fleet",
      "mining-colony",
      "research-station",
      "trade-fleet",
      "shellworld",
      "warp-portal",
    ];

    // See Page 5 of Eclipse: Rise of the Ancients rules.
    const MAX_DEVELOPMENTS = 8;
    const count = Math.min(MAX_DEVELOPMENTS, match.players.length + 1);
    match.developments.push(...Random.default.pick(developments, count));
  }
}

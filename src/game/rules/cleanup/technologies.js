import Random from "~/game/Random";
import TechnologyTrackId from "~/game/technologies/TechnologyTrackId";

function getCleanUpTechnologies(match) {
  switch (match.players.length) {
    default: throw new Error("Invalid amount of players, it should be >= 2 and <= 9");
    case 2:
      return 4;
    case 3:
      return 6;
    case 4:
      return 7;
    case 5:
      return 8;
    case 6:
      return 9;
    case 7:
      return 10;
    case 8:
      return 11;
    case 9:
      return 11;
  }
}

export default function cleanUpTechnologies(match) {
  // Gets from the bag the number of technologies required.
  let technologyCount = getCleanUpTechnologies(match);
  while (technologyCount > 0) {
    const technology = Random.default.takeOne(match.technologies);
    if (technology.track !== TechnologyTrackId.RARE) {
      technologyCount -= 1;
    }
    match.availableTechnologies.push(technology);
  }
}

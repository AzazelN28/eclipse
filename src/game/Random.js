export default class Random {
  static INCREMENT = 12345;

  static MULTIPLIER = 1103515245;

  static MODULUS = 2147483647;

  static default = new Random();

  static lcg(value, increment = Random.INCREMENT, multiplier = Random.MULTIPLIER, modulus = Random.MODULUS) {
    return ((value + increment) * multiplier) % modulus;
  }

  constructor(initialSeed = 0) {
    this.seed = initialSeed;
  }

  reset(newSeed = 0) {
    this.seed = newSeed;
    return this;
  }

  value() {
    this.seed = Random.lcg(this.seed);
    return this.seed / Random.MODULUS;
  }

  between(min, max) {
    return Math.round((this.value() * (max - min)) + min);
  }

  index(list) {
    return Math.round((list.length - 1) * this.value());
  }

  pickOne(list) {
    if (list.length === 0) {
      return undefined;
    }
    const index = this.index(list);
    return list[index];
  }

  pick(list, count) {
    if (count <= 0) {
      throw new Error("Invalid number of elements");
    }
    const result = [];
    for (let index = 0; index < count; index += 1) {
      const item = this.pickOne(list);
      if (item) {
        result.push(item);
      }
    }
    return result;
  }

  takeOne(list) {
    if (list.length === 0) {
      return undefined;
    }
    const index = this.index(list);
    const [removed] = list.splice(index, 1);
    return removed;
  }

  take(list, count) {
    if (count <= 0) {
      throw new Error("Invalid number of elements");
    }
    const result = [];
    for (let index = 0; index < count; index += 1) {
      const item = this.pickOne(list)
      if (item) {
        result.push(item);
      }
    }
    return result;
  }

  shuffle(list) {
    const clone = list.slice();
    let i = clone.length;
    while (i > 1) {
      i -= 1;
      const j = this.between(0, i);
      const tmp = clone[i];
      clone[i] = clone[j];
      clone[j] = tmp;
    }
    return clone;
  }
}

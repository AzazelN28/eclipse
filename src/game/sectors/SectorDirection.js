const SectorDirection = {
  UP: 0,
  RIGHT_UP: 1,
  RIGHT_DOWN: 2,
  DOWN: 3,
  LEFT_DOWN: 4,
  LEFT_UP: 5
};

/**
 * Uses Cube coordinates
 * @see http://www.redblobgames.com/grids/hexagons/#neighbors
 */

const SectorTileNeighbours = {
  UP:         [0, +1, -1],
  RIGHT_UP:   [+1,  0, -1],
  RIGHT_DOWN: [+1, -1,  0],
  DOWN:       [0, -1, +1],
  LEFT_DOWN:  [-1,  0, +1],
  LEFT_UP:    [-1, +1,  0],
};

const SectorTileInitial = [
  // 2 Player game
  [
    [0, 2, -2],
    [0, -2, 2],
  ],
  // 3 Player game
  [
    [0, 2, -2],
    [2, -2, 0],
    [-2, 0, 2],
  ],
  // 4 Player game
  [
    [2, -2, 0],
    [2, 0, -2],
    [-2, 2, 0],
    [-2, 0, 2],
  ],
  // 5 Player game
  [
    [2, -2, 0],
    [2, 0, -2],
    [-2, 2, 0],
    [-2, 0, 2],
    [0, 2, -2],
  ],
  // 6 Player game
  [
    [2, -2, 0],
    [2, 0, -2],
    [-2, 2, 0],
    [-2, 0, 2],
    [0, -2, 2],
    [0, 2, -2],
  ],
  // 7 Player game
  [
    [3, -1, -2],
    [-3, 1, 2],
    [-2, 3, -1],
    [2, -3, 1],
    [-2, -1, 3],
    [1, 2, -3],
    [0, -3, 3],
  ],
  // 8 Player game
  [
    [3, -1, -2],
    [-3, 1, 2],
    [-2, 3, -1],
    [2, -3, 1],
    [2, 1, -3],
    [-2, -1, 3],
    [0, 3, -3],
    [0, -3, 3],
  ],
  // 9 Player game
  [
    [3, -2, -1],
    [-3, 1, 2],
    [2, -3, 1],
    [-1, 3, -2],
    [-2, -1, 3],
    [1, 2, -3],
    [-3, 3, 0],
    [3, 0, -3],
    [0, -3, 3],
  ],
];

const SectorTileInitialWithAncientHomeworlds = [
  // 2 Player game
  [
    [],
    [],
  ],
  // 3 Player game
  [
    [],
    [],
    [],
  ],
  // 4 Player game
  [
    [],
    [],
    [],
    [],
  ],
  // 5 Player game
  [
    [],
    [],
    [],
    [],
    [],
  ],
  // 6 Player game
  [
    [],
    [],
    [],
    [],
    [],
    [],
  ],
  // 7 Player game
  [
    [],
    [],
    [],
    [],
    [],
    [],
    [],
  ],
  // 8 Player game
  [
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
  ],
  // 9 Player game
  [
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
  ],
];

export default SectorDirection;

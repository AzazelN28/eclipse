export default class Sector {
  constructor() {
    this.id = null;
    this.name = "empty";
    this.hasArtifact = false;
    this.hasDiscovery = false;
    this.hasWarpPortal = false;
    this.hasSupernova = false;
    this.hasPulsar = false;
    this.hasNebula = false;
    this.hasBlackHole = false;
    this.victoryPoints = 1;
    this.structures = [];
    this.ships = [];
    this.planets = [];
    this.wormholes = [];
  }

  get isCenterSector() {
    return this.id === 1;
  }

  get isInnerSector() {
    return this.id >= 100 && this.id < 200;
  }

  get isMiddleSector() {
    return this.id >= 200 && this.id < 300;
  }

  get isOuterSector() {
    return this.id >= 300;
  }
}

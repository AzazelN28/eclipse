import sectors from "~/game/data/sectors.yaml";
import Sector from "./Sector";

export default sectors.map((sector) => {
  return Object.assign(new Sector(), sector);
});

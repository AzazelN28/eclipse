import ActionType from "~/game/actions/ActionType";
import SectorDirection from "./SectorDirection";

export class SectorTilePosition {
  constructor(x = 0, y = 0, z = 0) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}

export default class SectorTile {
  constructor(sector, x = 0, y = 0, z = 0, direction = 0, influencedBy = null) {
    this.sector = sector; // reference to the sector.
    // maps the ships to REAL ship units.
    this.ships = sector.ships.slice();
    // Tile direction. If this value is up, then indicates
    // that the tile is not rotated.
    this.direction = direction;
    // TODO: See if it will be better to make this offset, cube or axial coordinates, by now I think that the better
    // approach is to use axial coordinates.
    this.position = new SectorTilePosition(x,y,z);
    // This will be similar to sector.planets
    this.population = [];
    // indicates which user has influenced this sector.
    this.influencedBy = influencedBy;

    // Eclipse: Supernova expansion.
    if (this.sector.hasSupernova) {
      this.supernovaExploded = false;
    } else {
      this.supernovaExploded = null;
    }

    // Eclipse: Pulsar expansion.
    if (this.sector.hasPulsar) {
      this.pulsarAction = ActionType.BUILD;
    } else {
      this.pulsarAction = null;
    }
  }
}

import Random from "./Random";

import MatchPhase from "./matches/MatchPhase";
import MatchDirection from "./matches/MatchDirection";
import Match from "./matches/Match";
import ActionType from "./actions/ActionType";

import prepareDevelopments from "~/game/rules/preparation/developments";
import prepareDiscoveries from "~/game/rules/preparation/discoveries";
import prepareSectors from "~/game/rules/preparation/sectors";
import prepareTechnologies from "~/game/rules/preparation/technologies";
import prepareReputation from "~/game/rules/preparation/reputation";

import cleanUpTechnologies from "~/game/rules/cleanup/technologies";

export default class Game {

  static getTechnologyDiscount(technologyTrackCount) {
    const trackValues = [0, -1, -2, -3, -4, -6, -8];
    return trackValues[technologyTrackCount];
  }

  static getTechnologyVictoryPoints(technologyTrackCount) {
    const trackValues = [0, 0, 0, 0, 1, 2, 3, 5];
    return trackValues[technologyTrackCount];
  }

  static getPopulationProduction(populationTrackCount) {
    switch (populationTrackCount) {
      default:
        throw new Error("Invalid amount of players, it should be >= 2 and <= 9");
      case 0:
        return 2;
      case 1:
        return 3;
      case 2:
        return 4;
      case 3:
        return 6;
      case 4:
        return 8;
      case 5:
        return 10;
      case 6:
        return 12;
      case 7:
        return 15;
      case 8:
        return 18;
      case 9:
        return 21;
      case 10:
        return 24;
      case 11:
        return 28;
    }
  }

  static getInfluenceCost(influenceTrackCount) {
    switch (influenceTrackCount) {
      default:
        throw new Error("Invalid amount of players, it should be >= 2 and <= 9");
      case 0:
        return 0;
      case 1:
        return 0;
      case 2:
        return 1;
      case 3:
        return 2;
      case 4:
        return 3;
      case 5:
        return 5;
      case 6:
        return 7;
      case 7:
        return 10;
      case 8:
        return 13;
      case 9:
        return 17;
      case 10:
        return 21;
      case 11:
        return 25;
      case 12:
        return 30;
    }
  }

  static prepare(match) {
    // Exceptions
    if (match.players.length < 2) {
      throw new Error("To start the match you need to specify at least 2 players");
    }

    // prepare game technologies
    prepareTechnologies(match);

    // prepare game discoveries
    prepareDiscoveries(match);

    // prepare game developments
    prepareDevelopments(match);

    // prepare game reputation
    prepareReputation(match);

    // prepare game sectors.
    prepareSectors(match);

    // Starts the game and sets the match phase to ACTION.
    match.phase = MatchPhase.ACTION;
    match.initialPlayerIndex = Random.default.index(match.players);
    match.playerIndex = match.initialPlayerIndex;
    match.player = match.players[match.playerIndex];
  }

  static actionPhase(match, action) {
    if (match.player === action.player) {
      // Player doesn't have any influence discs.
      if (match.player.influence === 0) {
        return false;
      }

      if (match.player.availableColonyShips - action.parameters.colonize.length > 0) {
        for (const colonize of action.parameters.colonize) {
          // TODO: Set population on planets.
        }
        match.player.availableColonyShips -= action.parameters.colonize.length;
      }

      // Remove influence discs from player.
      if (action.type !== ActionType.PASS) {
        match.player.influence -= 1;
      } else {
        match.player.hasPassed = true;
        if (match.players.reduce((acc, player) => acc + player.hasPassed, 0) === match.players.length) {
          // TODO: We should change state here.
          match.phase = MatchPhase.COMBAT;
          match.phase = MatchPhase.UPKEEP;
        }
        return;
      }

      switch (action.type) {
        default:
          throw new Error("Invalid action type");

        case ActionType.EXPLORE:
          action.parameters.positions = [];
          action.parameters.colonization = [];
          break;

        case ActionType.INFLUENCE:
          action.parameters.influences = [];
          break;

        case ActionType.RESEARCH:
          action.parameters.technologies = [];
          break;

        case ActionType.UPGRADE:
          action.parameters.upgrades = [];
          break;

        case ActionType.BUILD:
          action.parameters.builds = [];
          break;

        case ActionType.MOVE:
          action.parameters.units = [];
          break;
      }
    }
  }

  static combatPhase(match) {

  }

  static upKeepPhase(match) {
    for (const player of match.players) {
      const isBroke = player.getBalance() < 0;
      if (isBroke) {
        // TODO: If the player is broke, then we should calculate if he or she can remove influence discs or
        // convert some science or materials into money, if they can't they are marked as bankrupted.
      } else {
        // Mark the player is not broke then we mark him as ready.
      }
    }

    for (const player of match.players) {
      player.resources.science += Game.getPopulationProduction(player.population.science);
      player.resources.materials += Game.getPopulationProduction(player.population.materials);
    }
  }

  static cleanUpPhase(match) {
    cleanUpTechnologies(match);

    do {
      if (match.direction === MatchDirection.LEFT) {
        match.playerIndex = (match.playerIndex + 1) % match.players.length;
      } else if (match.direction === MatchDirection.RIGHT) {
        if (match.playerIndex > 0) {
          match.playerIndex -= 1;
        } else {
          match.playerIndex = match.players.length - 1;
        }
      }

      match.player = match.players[match.playerIndex];

    } while (match.player.hasLost);

  }
}

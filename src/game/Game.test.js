import Game from "./Game";
import Match from "./matches/Match";
import MatchSettings from "./matches/MatchSettings";
import fs from "fs";

test("should run the preparation", () => {
  const settings = new MatchSettings();
  const match = new Match(settings);
  Game.prepare(match);
});


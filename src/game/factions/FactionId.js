export const FactionId = {
  /* Base Game Factions */
  ANCIENTS: "ancients",

  /* Terran factions */
  DIRECTORATE: "directorate",
  UNION: "union",
  FEDERATION: "federation",
  REPUBLIC: "republic",
  ALLIANCE: "alliance",
  CONGLOMERATE: "conglomerate",

  /* Alien factions */
  ERIDANI: "eridani",
  PLANTA: "planta",
  HYDRAN: "hydran",
  DRACO: "draco",
  ORION: "orion",
  MECHANEMA: "mechanema",

  /* Rise of the Ancients factions */

  /** Terran-like factions but with variations */
  WARDENS: "wardens",
  SENTINELS: "sentinels",
  KEEPERS: "keepers",

  /* Alien factions */
  EXILES: "exiles",
  RHO_INDI: "rho-indi",
  LYRA: "lyra",

  /* Ship Pack One */
  SEPARATISTS: "separatists",
  GUARDIANS: "guardians",
};

export default FactionId;

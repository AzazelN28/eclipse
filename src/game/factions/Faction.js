export const FactionReputationType = {
  AMBASSADOR: "ambassador",
  REPUTATION: "reputation",
  BOTH: "both"
};

export default class Faction {
  constructor() {
    this.color = null;
    this.initialTechnologies = [];
    this.initialShips = [];
    this.initialInfluence = 0;
    this.initialSector = 0;
    this.initialResources = {
      money: 0,
      science: 3,
      materials: 4
    };
    this.colonyShips = 3;
    this.conversionRates = {
      moneyToScience: 3,
      moneyToMaterials: 3,
      scienceToMoney: 3,
      scienceToMaterials: 3,
      materialsToScience: 3,
      materialsToMoney: 3
    };
    this.reputationTrack = [];
    this.specials = [];
    this.ships = [];
    this.structures = [];
    this.explore = {
      canDraw: 1,
      canExplore: 1
    };
    this.influence = {
      canInfluence: 2,
      canRecoverColonyShips: 2
    };
    this.research = {
      canResearch: 1
    };
    this.upgrade = {
      canUpgrade: 2
    };
    this.build = {
      canBuild: 1
    };
    this.move = {
      canMove: 1
    };
  }
}

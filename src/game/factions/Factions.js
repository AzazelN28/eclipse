import factions from "~/game/data/factions.yaml";
import Faction from "./Faction";

export default factions.map((faction) => {
  return Object.assign(new Faction(), faction);
});

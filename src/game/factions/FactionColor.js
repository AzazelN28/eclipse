const FactionColor = {
  RED: "red",         // ERIDANI / TERRAN DIRECTORATE
  GREEN: "green",     // PLANTA / TERRAN UNION
  BLUE: "blue",       // HYDRAN / TERRAN FEDERATION
  YELLOW: "yellow",   // DRACO / TERRAN REPUBLIC
  BLACK: "black",     // ORION / TERRAN ALLIANCE
  WHITE: "white",     // MECHANEMA / TERRAN
  GREY: "grey",       // RHO-INDI / KEEPERS
  BEIGE: "beige",     // LYRA / SENTINELS
  PURPLE: "purple"    // EXILES / WARDENS
};

export default FactionColor;

import webpack from "webpack";
import NpmInstallPlugin from "npm-install-webpack-plugin";
import ExtractTextPlugin from "extract-text-webpack-plugin";
import HtmlPlugin from "html-webpack-plugin";
import path from "path";

function dist(...args) {
  return path.resolve(__dirname, "dist", ...args);
}

function src(...args) {
  return path.resolve(__dirname, "src", ...args);
}

const config = {

  entry: [
    src("index.js"),
    src("index.styl"),
  ],

  output: {
    publicPath: "/",
    path: dist(),
    filename: "index.js",
  },

  resolve: {
    extensions: [".js", ".jsx"],
  },

  module: {
    rules: [
      {
        test: /\.ya?ml$/,
        use: ["json-loader", "yaml-loader"],
      },
      {
        test: /\.json$/,
        use: "json-loader",
      },
      {
        test: /\.styl$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: "css-loader",
              options: {
                module: true,
              },
            },
            "stylus-loader",
          ],
        }),
      },
      {
        test: /\.jsx?$/,
        use: "babel-loader",
      },
      {
        test: /\.pug$/,
        use: "pug-loader",
      },
    ],
  },

  plugins: [
    new NpmInstallPlugin(),
    new ExtractTextPlugin("index.css"),
    new HtmlPlugin({
      title: "Eclipse: Online",
      template: src("index.pug"),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.UglifyJsPlugin(),
  ],

  devServer: {
    contentBase: dist(),
    port: process.env.PORT || 3000,
    hot: true,
    compress: true,
    historyApiFallback: true,
  },

};

export default config;

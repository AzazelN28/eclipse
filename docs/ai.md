# Over 2 million different AIs for Eclipse.

(version 1.1)

By Vica8081

## Index
0. Updates
1. Introduction
2. Main Traits
3. System mechanics
    1. Peace Flow
    2. War Flow (attack)
    3. War Flow (defend)
4. Phase explanation
5. Appendix.
    - Notes on several alien races
    - AI random generator

## 0. Updates

V1.1 -> Added “optional ancients attack” phase in the peace flow. After the last playtesting I realized that one of the main reasons of human player doing better than the AIs is because they are too lenient with Ancients. This phase should fix it, because it makes that they prefer to attack ancients and ocuppy their hex, than to explore and discover a probably worse hex, although is not tested yet. Other minor fixes, as a recomedation on the AI random generator

## 1. Introduction

Yes, you have read the title correctly, over 2 million different AI for Eclipse (base game) so you can play against them or just let them fight one each other. The AIs don’t cheat and play exactly as any other player. The system is based in several independent traits. The main trait is the behavior, which determines the ship movement and the exploration of the player. here a lots of other minor traits, which change the way to design ship, what to do with discoveries or how to build the fleet among other things. The system is modular in the sense that you can just pick the main trait and forget everybody else, or use some traits and other not, whatever is fine with you. Is not a exhaustive AI what means that it didn’t determine exactly every movement of the player but instead points you and guide you to most of the decisions, but at the end you are also taking decisions and in fact you are welcome to override the AI if you think is suggesting to do something stupid (if they are able to build starships they can´t be as stupid to do whatever the AI suggest).

## 2. Main Traits

The most important thing in the system is the main trait. It defines overall behavior of the AI. Although some main trait are depicted to begin with war from the beginning (RISK, offensive, medal collector) to begin the “war flow (attack)” a player need to have at least one dreadnought or two cruisers, so until they are built the AI would remain in the “peace flow” (if not attacked) and to determine which is the weaker player, the AI would sum how much is cost every ship and starbase of the player and add one material for each upgrade they have (better than its).

There are ten different main traits:

**Pacifist:** Would explore sector 2, then ector 3, then 2 if available, 1 if available, then 3. Would try to exchange diplomat with every player it meet, would never start a war. If attacked two option (flip a coin) would fight to death with their aggressor or just until it get one hex of the opponent and then would try to settle peace. Monolith and orbitals are priority researches for a pacifist.
*Roleplaying notes:* This AI likes to play multiplayer solitaires and don’t want to get messed up with battles, just grow as much as you can and then build monolith. Not the funniest AI but it serves as a benchmark to others.

**Turtle:** Would explore sector 2, then sector 3. The AI would always try to put the explored hex so it became isolated of the rest of players. Would try to exchange diplomats with every player it meet, and would never start a war until it would be unable to explore further. Then will go out of its shell and attack the weakest neighbor opponent. Monolith and orbitals are priority researches for a turtle. If attacked when there is still exploration available would fight until it get one hex of the opponent and then would try to settle peace, when there is not exploration would fight to death.
*Roleplaying notes:* This AI plays a lot of starcraft games so it like to grow undisturbed at the beginning and when is have gotten big and powerful, storm into the rest galaxy. Is not a bad strategy so if left unchecked could easily get the victory,
so it is a good AI to play against

**Opportunist:** Would explore sector 2, then sector 3, then 2 if available, 1 if available, then 3. Would try to exchange diplomat with every player it meet, but would attack to the closest player which is already involved in any war (so if B attack C the opportunist would attack B or C according which is closer to it, if both are close, would attack the weakest player). Would only have one war at time and would seek for peace if things are going bad.
*Roleplaying notes:* This AI plays a lot of diplomacy games. It seems a pacifist but then in the worst moment, when you have your ships far away, backstab you and grab your precious system. In my playtesting (quite limited) is the more successful AI, so also a good AI to play against

**Greedy Neighbor:** Would explore sector 2, then sector 3, then 2 if available, 1 if available, then 3. Would try to exchange diplomat with every player it meet, but if at any time one neighbor player have less than 66% of its own fleet would attack. If the other player have less than 80% of its fleet and the technology is worse in at least two components (shield, computer, guns, etc.) would also attack. If there is not exploration available, would attack its weakest neighbor. Would only have one war at time and would seek for peace if things are going bad
*Roleplaying notes:* This AI is also a wolf in a sheep (pacifist) dress. I´m now playtesting to fuse Pacifist/Opportunist/Greedy Neighbor as one single AI that act as pacifist but when opportunity arise you roll a die to determine if this blue player is really a pacifist (1-2) so don’t attack you, or (3-4) opportunist consider you backstabbed or (5-6) a greedy neighbor don’t forget keep your army full. This way the human player couldn’t possibly know from the beginning what AI player to trust and what AI player to watch.

**Cleaner:** Would explore sector 2, then sector 1, then sector 2 if available, 1 if available, then sector 3. Would try to exchange diplomats with every other player, and don’t get involved in any war with them but hate ancients and try to kill as many ancients as possible. First would kill all the ancients in their borders but when the only remaining ancients are behind other players then war is the only option, but would try to seek peace as soon as the ancients are destroyed/the pass is achieved. When/if all ancients are destroyed the cleaner would behave as a pacifist and could even get an optional bonus of VP (maybe 5?)
*Roleplaying notes:* This AI is another of the winning AI in my (limited) playtesting, and in fact is a multiplayer solitaire player looking for some action, although you can also imagine them as a former slaved races of the ancients or something like that

**Offensive:** Would explore sector 2, then sector 1, then sector 2 if available, 1 if available, then sector 3. Would never exchange diplomats and would attack the weaker player close to it, although it would have only one war at time. If thing are going bad, would try to make peace and attack other neighbor player (but only if the other player is seen as weaker)
*Roleplaying notes:* This AI think that the only acceptable form of life it is their own, (like Borgs) so every player is an abomination and a enemy, although is wise enough to attack them one at time, but I doubt it can ever win a game of Eclipse. Noneless is a fun AI to play

**Medal collector:** Would explore sector 2, then sector 1, then sector 2 if available, 1 if available, then sector 3. At first would attack the weaker player close to it. Then will exchange diplomats with the rest. When it have an average of at least 2,5 points in every medal space, would switch to a pacifist behaviour (with orbital and monolith as priority technology included) and try to make peace. If thing are going bad, and still under the 2,5 average would try to make peace and attack other neighbor player (but only if the other player is weaker).
*Roleplaying notes:* This AI is a eclipse gamer. It try to get early combat points and at soon as the combat points provide little chance to increase (the 2.5 average) switch to a pacific behavior. However in my (limited) playtesting this AI have never won.

**Preventive attack:** Would explore sector 2, then sector 1, then sector 2 if available, 1 if available, then sector 3. At first would exchange diplomats with others players, but when two diplomats are exchanged would attack to death to the next player which become contacted. Would also attack to death to any player which have not exchanged diplomats (the first version would wait to exchange it 3 diplomats before war, but in playtesting it would never contact a 4th player, so it was changed, but if you play with 8-9 players, maybe the first version is better, and more in spirit with the AI). If only contacted two players (so not war) and there is not exploration available, would attack the last player contacted.
*Roleplaying notes:* This AI is a bit paranoid so if it is unable to exchange diplomats, it would fear that the other player is going to attack them, so it prefer to give the first blow without asking.

**RISK (Color):** A risk player would hate the player with that specific color (so a RISK (red) would try to kill and annihilate the red player). The AI would explore to get connected to the hated player, and then would only explore sector 3. Would exchange diplomats with the other players, but attack to death to its objective. A RISK player doesn’t care about VP, planets controlled or what the other players are doing, its only objective is to rid the galaxy of the horrible menace of red color, so in some ways a RISK player is not really playing eclipse and a moral victory (or bonus points) is achieved if the RISK player is able to completely destroy its hated color, and save the galaxy of this awful menace. When/if that is achieved the RISK player would switch to a pacifist behavior (with orbital and monolith as preferred technologies).
*Roleplaying Notes:* This AI is clearly playing the wrong game. It is fun to watch, and if you want and additional challenge you can put one (or even two) risk player against yourself. If you are able to survive, or even to win the game, then you are a damn good player.

**Gamer:** Would explore sector 2, then sector 1, then sector 2 if available, 1 if available, then sector 3. A gamer in the other hand is playing eclipse. At beginning of each turn it would count the VP of every player. If nobody have the same or more than its, would follow the peace flow, and monolith (not orbitals) would be preferred technologies. If somebody have equal or more VP than its, then would attack the player with most VP, because it’s the leader and is going to win (not matter than its army is double than him, for example). Would never make peace with somebody with more VP than its (although is losing) because its didn’t like to become second.
*Roleplaying notes:* This AI is clearly a real gamer. In real life nobody in its own volition would attack the strongest country (so is bigger and stronger than you) just because they are the strongest. People tend to pick the weaker player and there is a reason to. So a gamer alone is doomed, but a gamer and a couple of opportunist or several gamers can make an interesting challenge. It make also a good AI to play against because is like a RISK but only goes against you when you are strong enough and not from the beginning. And at least have a fair reason to fight you, not just I hate you, red color.

## 3. System Mechanics

In turn one, every AI would perform exploration until it owned three hexes (the original one, and two new ones), and then would start to follow the “peace flow” (below). In subsequent turns, at the beginning of each turn, and depending on its main trait and the ship placement on the map, the AI must decided if they are going to attack (o being attacked) or they are gonna have a pacific turn. According to that decision, the AI would follow the “peace flow” or the ”war flow”:

The AI decide is being targeted whenever other player (without diplomatic relations) build or move more than 1 fighter in a neighbor hex, then AI will switch to the “war flow” (if they was not already in there). If the other player have exchanged diplomatics, then the state alert only arise if the enemy moves (or build) 2 cruiser or 1 dreadnought (or more) in a neighbor hex. If the turn finished and the opponent have not attacked (something not unusual, for example AI see you have no ship in the sector, moves close to attack, then the other AI shift to “war flow” and build a dreadnought and a starbase, and in the new conditions opponent choose not to attack) then the red alert end, and the next turn would begin as the “peace flow”.

### Peace flow:

1. Research
2. (Optional) Upgrade
3. (Optional) Attack Ancients
4. Exploration
5. Build

The AI would follow the peace flow, until he can´t take any action because it is unable to fulfill the requeriments for any of them or because one action more would mean bankrupt. The only exception would be if the AI own a hex without planets, in this case the AI would take one additional action so it can recover the disk in the hex without planets. Additionally, if at any time, the AI could use a influence action to get control of a hex with planets, it would do, doesn’t matter in what point of the peace or war flow would be. (i.e. the correct peace flow would be

1. (Optional) influence
2. Research
3. (Optional) influence
4. (Optional) Upgrade
5. (Optional) influence
6. (Optional) Attack Ancients
7. (Optional) influence
8. Exploration
9. (Optional) influence
10. Build

War flow (both defensive and attack side)

1. Build
2. Move (as many times as needed but leaving at least one action available after)
3. If there are two actions available: research, then upgrade
3. If there are only one action available: upgrade

In the case of the flow war, the AI wouldn’t mind to being bankrupt and lose one system if its being attacked, so it could take one extra action in this case to help its defense. (If the AI lose the battle it would not be bankrupt after all) The AI could follow the flow taking only one action on each point (thoroughly trait) and going back to the start when finish or taking as many action as possible in each point whenever meet the requirement (obsessive trait). This way in the same conditions a thoroughly AI would take one research action, one upgrade, one exploration and one build and then start again, and one obsessive AI could made two research action if possible, then one upgrade, one exploration and then stop because is too low on money to take the build action.

## 4. Phase explanation

### Peace Flow

#### 1. Research.

If available the player would take the “priority technology” which is less abundant, in case of an abundance tie, would pick the most expensive (without taking account discounts), and in a cost tie would pick the tech with higher discount. If the tie persist would pick the tech in the higher row (military first, then energy, then production). If there are not any available “priority technology” the AI would pick the most expensive technology available (without taking account discounts), in case of tie the tech with higher discount, if the tie persist the less abundant, and if everybody else fails, the tech in the higher row (same as above). As a exception, an AI would never research a tech which it already have a better version (ie computer +2 if already have computer +3 or neutron guns if have antimatter guns) except if is the only option AND would improve the points in its tech chart

If the cheapest priority tech is one science too expensive, the AI would exchange 3:1 to get the priority tech instead of a regular tech The “priority technology” are: Robotics, Quantum Grid, Starbase, Neutron bombs, Advanced armor, advanced Labs, Economy or Production if there are at least two empty planets of that kind in its controlleds hex, any ship technology of its preferred upgraded (ie if its preferred upgraded are computers, both computer gluon and electronic would be “priority technology”), and any Source Power if the possible ships model are being handicapped by insufficient power.

#### 2. Optional upgrade.

The optional upgrade phase would take place only after several parts of ship are being researched. The trait “tech addict” would take the upgrade action any time a new part of ship is researched. The trait “balanced” would take the upgrade action when two new parts of ship are researched, and the trait “budget limited” would take the upgrade action when three new parts are researched. There are 8 different traits which define how the AI would upgrade its ship:

| Trait | Fighter | Cruiser/Starbase | Dreadnought |
|:---:|:---:|:---:|:---:|
| True Balanced | Most expensive upgrade | 1 gun 1 armor 1 comp 1 shield | 1 gun 1 missile 2 armor 1 comp 1 shield |
| Balanced Armor | Most expensive upgrade | 1 gun 1 armor 1 comp 1 shield | 1 gun 3 armor 1 comp 1 shield |
| Balanced Guns | Most expensive upgrade | 1 gun 1 armor 1 comp 1 shield | 1 gun 2 missile 1 armor 1 comp 1 shield |
| Specialized guns | Extra gun | 1 gun 1 missile 1 armor 1comp | 1 gun 3 missiles 1 armor 1 comp |
| Specialized armor | Extra armor | 1 gun 2 armor 1 comp | 1 gun 4 armor 1 comp |
| Specialized computer | Computer | 1 gun 1 armor 2 comp | 2 gun 1 armor 2 comp 1 shield |
| Specialized shields | Shield* | 1 gun 1 comp 2 shield | 2 gun 1 armor 1 comp 2 shields |
| Specialized engines | Extra engines | 1 gun 1 armor 1 comp 2 engines | 2 gun 1 armor 1 comp 1 shield 2 engines |
> *If not have any shield researched, then the most expensive upgrade

If there are not missile researched they are replaced by guns.

Of course if it is impossible to upgrade any of the designs the phase will be skipped.

#### 3. (Optional) Attack Ancients

If the AI have **at least two cruiser or one dreadnought** and there is **an ancients ship** in a **neighbor hex**, the AI would **move this ship to combat the Ancients**.

The AI is wise enough to have previously built the ships close to the ancients if there are not any other menaces (i.e. another player activating war flow defensive).

If the AI have never upgrade its ships the next action would be an upgrade. (Although you are welcome to make a trait on this, with just one upgrade and 2 cruiser or 1 dreadnought against 1 ancients the fight should be almost a sure win).

If there two ancients the forces should be doubled (and at least two upgrades).

#### 4. Exploration.

If the AI have another available disk (ie not bankrupt by using it) AND there are at least one available colony ship, then the AI would explore. If there are neither disk nor colony ship available the AI would skip this phase. If there only one of the
conditions, the AI would explore based on its trait:

“Grow and prosper” Always explore “We are the biggest” Explore only if at least other player have equal or more systems “We aren’t the smallest” Explore only if every other player have equal or more systems

The type of hex explored (I,II or III) would depend on its main trait.

The AI would discard only if there are two ancient ships present (except descendants, of course), optionally you could make different traits (“never” “when there are ancients ships” “when there are two ancients ships”)

The AI would use the discoveries according this other trait (“alway VP” “always VP unless is a ship upgraded” “never VP”)

To decide what kind of production to put in gray planets and diplomats, the AI would use one of the following traits “Always economy” “Always research” “Always material” “Balanced” “ERM”, “EMR” “REM” “RME” “MER” “MRE”, The first three would always put that type of production, no matter how much (or how little) they produce in the other rows. The balanced trait would try to balance the three productions, so always would put the production that is lower. If there is a tie, throw a coin. In the other six traits the AI would try a stepped production which means that the AI would try to produce one step more of the first letter than middle and one step more the middle than the last letter (so in REM, if you research is 12, your economy is 10 and material 8 is a OK situation for the AI), but noneless the AI would colonize every planet that is able to, although it goes against their preferences, is better to produce something you didn’t like than not produce any at all. This trait is only to modulate how the AI use gray planets and diplomat to balance its production, not to limit the planet colonization. If they have to break the already stepped production (for example the AI in the previous case colonize another gray planet, so its “perfect” step production is broken) they would put the lower production first. (so they get research 12 material and economy 10, in the next gray colonization research and economy 12 material 10, and on the next one research 15 economy 12 material 10 (again “perfect situation” for the AI)

#### 5. Build

The AI would take this action only if it have enough materials to build what it desire to build (according to the following trait) If the AI is one material short of his goal, then it can exchange 3:1 to be able to build.

The type of ship produced depends of its trait (see table below)

| Trait | Production | Next production | Next production |
|:-----:|:----------:|:---------------:|:---------------:|
|Balanced Dreadnought | Dreadnought + starbase | Cruiser + starbase | 2 cruisers
| Balanced Cruiser | Cruiser + starbase | Dreadnought | |
| Ultradefensive Cruiser | Fighter + starbase | 2 cruisers |Dreadnought |
| Ultradefensive Dreadnought | Fighter + starbase | Dreadnought | 2 cruisers |
| Defensive Cruiser | 2 starbases | 2 cruisers | Dreadnought |
| Defensive Dreadgnouth | 2 starbases | Dreadnought | 2 cruisers |
| Offensive Cruiser | 2 cruisers | Dreadnought | |
| Offensive Dreadnought | Dreadnought | 2 cruisers | |
| Offensive Mix | Dreadnought + Cruiser | 2 cruisers | |
| Mobile Cruiser | Cruiser + fighter | Dreadnought + fighter | 2 fighters |
| Mobile Dreadnought | Dreadnought + fighter | Cruiser + fighter | 2 fighters |
| Offensive Dreadnought extra | Dreadnought + fighter | 2 cruisers | 2 fighters |

In case the AI can build a third ship, follow the table to get an idea that what they
would want.

The next production would only activate if the previous build is impossible to achieve (because all the models of that kind of ship have been already build or because the technology needed is absent in the case of 2 starbases)

The AI would place two starbase in their home system and the other two in two different frontiers system (if it is have only one frontier system, then both in the same hex). If the AI produce fighters, would place one fighter in each owned system and would use fighter as a defensive force only.

### War Flow (defensive mode)

#### 1. Build

The AI would build (if possible) 2 starbases in the hex being attacked and if unable to it, the 2 biggest possible ships with the material available, willing to spend even 2 times the 3:1 exchange if needed.

#### 2. Move

The AI would count as many action at it can spend, and try to get as many and as bigger ship as possible to the hex being attacked (or menaced) remember to let at least one action free for upgrade. If it could let two actions free or get two ship more involved, then the trait “Big force” prefer to move the ship and have only one free action and the trait “Superior technology” prefer to let two actions free (so the AI could research in this turn) and not move the ship. Remember in the war flow the AI could get in bankrupt if needed (being attacked), but not is it is only being menaced

#### 3. Optional Research

The AI would research if there are two actions free after movement. The AI in this phase is the same that the research action of the peace flow with the following limitation: The AI would only research ship components and neutron bombs (if attacking), so the priority technologies are now only any specialized component according its upgraded trait, or power supply if the ideal upgrade is being limited by low power. Would exchange until 2 times the 3:1 if needed

#### 4. Upgrade phase

The AI would upgrade according its trait but taking in account that only ships in the hex attacked can be upgraded (so if there are not cruisers in the hex attacked, cruisers can´t be upgraded) If there are enough actions (after build and movement) would take this action as many time as possible until bankrupt one system or ideal ship achieved.

### War flow (attack mode)

Remember the AI need at least one dreadnought or two cruisers to be in this mode

#### 1. Build
The AI would build according its trait in the nearest hex to the target. Would build several times is material allows it. Would only exchange 3:1 one time.

#### 2. Move
The AI would move all its available dreadnought and cruisers to its hex target. Remember to let at least one free action after movement. The traits "Big force" and "Better technology" applies

#### 3. Optional Research and 4. upgrade phase
Proceed as above, but only one 3:1 exchange

Appendix:

A. Notes on several aliens race:

The eridani empire would take actions until its debts is 9 or higher every turn (or bankrupt, its is have less than 9 economy).

The Draco Descendant can´t be played as the cleaner main trait or the “discard when ancient ships” trait (obviously) and when they chose the hex to explore they would choose the hex with more ancient ships if tied the hex with more planets, if tied the hex with more points/artifact, if tied the first hex picked.

The hydran progress would try to get two research done is possibly so first would pick the cheapest available one (if priority research better, of course) and then would look if there are enough research for another purchase. If not, then they would NOT pick the cheapest one and they use all its funds to get a research more expensive (as the rest of AIs do)

The planta player would decide to explore or not based in to be able to occupy at least one hex, so it don’t need to have two influence disk available and ready colonization ships to get a sure exploration, just one influence disk available and one
colonization ship ready, as everybody else.

B. AI random generator.

Roll two D10, a D12, a D8 and three D6 (the yellow, red and orange) and look this:

| D8 (upgrade trait) | D10 (main trait) | D12 (building trait) | D6 yellow (rate of upgrade & chart flow) | D6 orange (exploration & war) | D6 red (discard & discoveries) |
|:---:|:---:|:---:|:---:|:---:|:---:|
| True Balanced | Pacifist | Balanced Dreadnought | Tech addict & Obsessed | Grow and prosper & superior tech | Never & Always VP |
| Balanced Armor | Turtle | Balanced Cruiser | Tech addict & Thoroughly | Grow and prosper & big force | Only 2 Ancients & Always VP |
| Balanced Guns | Opportunist | Ultradefensive Cruiser | Balanced & Obsessed | We are the biggest & superior tech | Never & Always VP except upgrade |
| Specialized guns | Greedy Neighbor | Ultradefensive Dreadnought | Balanced & Thoroughly | We are the biggest & big force | Only 2 ancients & Always VP except upgrade |
| Specialized armor | Cleaner | Defensive Cruiser | Budget Limited & Obsessed | We are not smallest & superior tech | Never & Never VP |
| Specialized computer | Offensive | Defensive Dreadgnouth | Budget Limited & Thoroughly | We are not smallest & big force | Only 2 ancients & Never VP |
| Specialized shields | Medal collector | Offensive Cruiser |
| Specialized Engines | Preventive attack | Offensive Dreadnought || Always economy | Stepped EMR |
|| RISK (roll another die for color) | Offensive Mix | D10 (Gray planets & diplomats) | Always Research | Stepped REM |
|| Gamer | Mobile Cruiser || Always Material | Stepped RME |
||| Mobile Dreadnought || Balanced | Stepped MER |
||| Offensive Dreadnought Extra || Stepped ERM | Stepped MRE |

If you do the math that means over 2 millions of different AIs, and the “discard with one ancient” trait is not even present or the six potential different RISK players (one for each color). And there is more, is not the same to play against 5 offensice player than 2 opportunist one gamer and two turtle, or… so the different combinations grow exponentially.

My recommendation is to pick the main traits you want to battle with (for example 2 opportunists, 1 cleaner, 1 turtle and 1 gamer), and then roll the rest of traits.

Enjoy and please comment, suggest new traits, give thumbs, etc.

Special thanks to Touko Tahkokallio and all the people on lautapelit.fi for creating so
great game.

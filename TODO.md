# TODO

- [ ] Refactor Game file because it is growing very fast and it should be splitted into smaller files.
- [ ] Matches
  - [ ] Preparation phase
    - [ ] Prepare initial sectors ships (modify sectors.yaml and include them there)
    - [ ] Prepare initial sectors population
    - [ ] Prepare initial ancient homeworlds
  - [ ] Action phase
  - [ ] Up Keep phase
  - [ ] Clean Up phase

- [ ] SVG Icons and Assets (Hex Tile, Icons, etc)
  - [ ] Diplomacy
    - [ ] Ambassador
    - [ ] Reputation
    - [ ] Both (Reputation and Ambassador)
  - [ ] Technologies
    - [ ] Neutron Bombs
    - [ ]
  - [ ] Hex Tile
    - [ ] Wormholes
    - [ ] Planet
    - [ ] Advanced Planet
    - [ ] Warp Portals
    - [ ] Supernova
    - [ ] Pulsar

- [ ] Images
  - [ ] Faction
    - [ ] Terran Directorate
    - [ ] Terran Federation
    - [ ] Terran Union
    - [ ] Terran Republic
    - [ ] Terran Alliance
    - [ ] Terran Conglomerate
    - [ ] Terran Separatists
    - [ ] Magellan Guardians
    - [ ] Magellan Keepers
    - [ ] Magellan Wardens
    - [ ] Magellan Sentinels
    - [ ] Eridani Empire
    - [ ] Hydran Progress
    - [ ] Planta
    - [ ] Descendants of Draco
    - [ ] Orion Hegemony
    - [ ] Mechanema
    - [ ] The Exiles
    - [ ] Rho-Indi Syndicate
    - [ ] Enlightened of Lyra



  -

- [ ] Components
  - [ ] Checkbox
  - [ ] Input
  - [ ] Slider
  - [ ] List
  - [ ] Table

- [ ] Screens
  - [ ] Match Creation Screen
  - [ ] Game Screen
  - [ ] Map
